import RPi.GPIO as GPIO
import time
import serial

# GPIO pins for motor control
MOTOR1_PIN1 = 17
MOTOR1_PIN2 = 27
MOTOR2_PIN1 = 23
MOTOR2_PIN2 = 22

# GPIO mode and pins
GPIO.setmode(GPIO.BCM)
GPIO.setup(MOTOR1_PIN1, GPIO.OUT)
GPIO.setup(MOTOR1_PIN2, GPIO.OUT)
GPIO.setup(MOTOR2_PIN1, GPIO.OUT)
GPIO.setup(MOTOR2_PIN2, GPIO.OUT)

# Serial communication with Bluetooth module
bluetooth_port = "/dev/rfcomm0" 
baud_rate = 9600
bluetooth_serial = serial.Serial(bluetooth_port, baud_rate, timeout=1)

# Move based on remote control input
def control_sleigh(command):
    if command == 'F':
        move_forward()
    elif command == 'B':
        move_backward()
    elif command == 'L':
        turn_left()
    elif command == 'R':
        turn_right()
    elif command == 'S':
        stop_sleigh()

# Forward movement
def move_forward():
    GPIO.output(MOTOR1_PIN1, True)
    GPIO.output(MOTOR1_PIN2, False)
    GPIO.output(MOTOR2_PIN1, True)
    GPIO.output(MOTOR2_PIN2, False)

# Backward movement
def move_backward():
    GPIO.output(MOTOR1_PIN1, False)
    GPIO.output(MOTOR1_PIN2, True)
    GPIO.output(MOTOR2_PIN1, False)
    GPIO.output(MOTOR2_PIN2, True)

# Left movement
def turn_left():
    GPIO.output(MOTOR1_PIN1, True)
    GPIO.output(MOTOR1_PIN2, False)
    GPIO.output(MOTOR2_PIN1, False)
    GPIO.output(MOTOR2_PIN2, True)

# Right movement
def turn_right():
    GPIO.output(MOTOR1_PIN1, False)
    GPIO.output(MOTOR1_PIN2, True)
    GPIO.output(MOTOR2_PIN1, True)
    GPIO.output(MOTOR2_PIN2, False)

# Zero movement
def stop_sleigh():
    GPIO.output(MOTOR1_PIN1, False)
    GPIO.output(MOTOR1_PIN2, False)
    GPIO.output(MOTOR2_PIN1, False)
    GPIO.output(MOTOR2_PIN2, False)

try:
    while True:
        # Bluetooth command reading
        command = bluetooth_serial.read().decode('utf-8')

        # Received command control
        control_sleigh(command)

except KeyboardInterrupt:
    GPIO.cleanup()
    bluetooth_serial.close()
